
Zde zpusoby použité k získání referencí a nástrojů

* Salmon - staženo z https://github.com/COMBINE-lab/salmon/releases verze 1.9.0
* Referenční transkriptom - staženo z ensembl.org (https://ftp.ensembl.org/pub/grch37/current/fasta/homo_sapiens/cdna/)  (pripadne mozno pouzit gffread k sestaveni z genomu a gff)
* sleuth -  BiocManager("rhdf5")
            devtools::install_github("pachterlab/sleuth") (in R console)
* citr - devtools::install_github("crsh/citr")

