# Bakalarska prace F. Kloda #

Tento adresar slouzi pro vsechny soubory spojene s mou bakalarskou praci.
Prace bude zpracovavana pomoci balicku bookdown. Jednotlive kapitoly budou tedy ulozene jako jednotlive .md soubory.

## Struktura ##

* /sources.md - soubor pro zapisovani jednotlivych zdroju, ktere by se hodilo precist, na kterych se pracuje, ktere uz jsou zahrnuty.  
Obsahuje odkazy na dane zdroje. 

* /input_data - adresar obsahujici vstupni data pro analyzu (skripty)
* /scripts - adresar pro .md soubory reprezentujici jednotlive kapitoly prace (a zaroven i dalsi skripty)
* /sources - adresar pro soubory obsahujici "vycucy" z jednotlivych zdroju obsahujici i odkaz na dany zdroj.  
Jeden soubor na jeden zdroj.

