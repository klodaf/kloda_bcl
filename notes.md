* Read vignette
* Search on PubMed
* Look for other articles from the same author
* Check articles cited in current article
* .fastq format = output of sequencing, made of short reads, raw data
* .fasta format = format for a whole sequence
* sequencing depth - number of reads per sample made during sequencing  
* Types of alternative splicing:  
    * Exon cassette - 3 exons, AS is made by cutting out the middle exon (48% of AS in humans)  
    * Alternative 5'/3' splice site - different splice sites inside of an exon (37% of AS in humans)  
    * Mutually exclusive exons - similar to exon cassette, multiple exons but only some combinations are allowed  
    * Intron retention - Intros are not ut out  
* Tools that are being used: Kallisto, Cufflinks, Salmon, DEXseq, RSEM(maybe)  
* Always write all commands I use (even for downloading a package)  


* Do I need to convert all results to exon/isoform expression????

Samtools functions:  
* view - views the given bam/sam file. Optionally chromosome may be specified (samtools view input.bam chr1)
